from django.contrib import admin
from myApp.models import attendance,employee,emp_details,HR,document,leave,holiday,Notification,leaveAssign,leaveApply,announcement,event,HrNotification,attendance_user

class emp_detailsAdmin(admin.ModelAdmin):
    list_display=('id','emp_name')
class attendanceAdmin(admin.ModelAdmin):
    list_display=('id','check_in_time','check_out_time')
class attendance_userAdmin(admin.ModelAdmin):
    list_display=('id','date','time', 'attendance_type')

admin.site.register(employee)
admin.site.register(emp_details,emp_detailsAdmin)
admin.site.register(HR)
admin.site.register(document)
admin.site.register(leave)
admin.site.register(Notification)
admin.site.register(leaveAssign)
admin.site.register(leaveApply)
admin.site.register(announcement)
admin.site.register(holiday)
admin.site.register(attendance,attendanceAdmin)
admin.site.register(event)
admin.site.register(HrNotification)
admin.site.register(attendance_user, attendance_userAdmin)



