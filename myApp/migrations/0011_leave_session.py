# Generated by Django 2.1.7 on 2019-04-29 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myApp', '0010_auto_20190427_1532'),
    ]

    operations = [
        migrations.AddField(
            model_name='leave',
            name='session',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
