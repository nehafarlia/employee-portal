# Generated by Django 2.1.7 on 2019-04-29 09:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myApp', '0012_leave_sub_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leave',
            name='status',
            field=models.CharField(blank=True, choices=[('Approve', 'Approve'), ('Pending', 'Pending'), ('Rejected', 'Rejected')], default='Pending', max_length=10, null=True),
        ),
    ]
